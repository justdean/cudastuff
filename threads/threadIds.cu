#include <stdio.h>

__global__ void getThreadsId()
{
  printf("%i\n", threadIdx.x);
}

__global__ void getDimensions()
{
  printf("%i\n", blockDim.x);
}

int main()
{
  printf("Outputs thread IDs of 16 threads in 1 block\n");
  getThreadsId<<<1,16>>>();
  cudaDeviceSynchronize();
  printf("Outputs the number threaads in a block\n");
  cudaDeviceSynchronize();

  return 0;


}
