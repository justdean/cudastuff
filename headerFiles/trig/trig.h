#include <stdio.h>

#ifndef trig
#define trig

double sine(double X);
double cosine(double X);
double arctangent(double X, double Y);

#endif


