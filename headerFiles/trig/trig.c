#include "trig.h"
#include <math.h>

#define N 20
double factorial(double n);

double sine(double X)
{
  double X_N = 0;
  for(int n = 0; n < N; n++)
    {
      double fact_n = factorial(2*n+1);
      X_N += (pow(-1, n)*pow(X, (2*n +1)))/fact_n;

    }
  return X_N;

}

double cosine(double X)
{
  double X_N = 0;
  for(int n = 0; n < N; n++)
    {
      double fact_n  = factorial(2*n);
      X_N += (pow(-1, n)*pow(X, 2*n))/ fact_n;
    }
  return X_N;
}

double arctangent(double X, double Y)
{
  double theta = Y/X;
  double X_N = 0;
  for(int n = 0; n < 1000000; n++)
    {
      X_N += (pow(-1, n) * pow(theta, 2*n+1))/(2*n+1);
    }

  return X_N;

}

double factorial(double n)
{
  double I_N = 1;

  for(int i = 1; i <= n; i++)
    {
      I_N *= i;

    }
  return I_N;
}

