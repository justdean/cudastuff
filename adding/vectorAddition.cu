#include <stdio.h>

__global__ void add(int *a, int *b, int *c)
{
  c[blockIdx.x] = a[blockIdx.x] + b[blockIdx.x];
}

#define N 512

void random_ints(int* a, int N)
{
   int i;
   for (i = 0; i < N; ++i)
    a[i] = rand();
}

int main(void)
{
  int *a, *b, *c;
  int *d_a, *d_b, *d_c;

  int size = N * sizeof(int);

  cudaMalloc((void **)&d_a, size);
  cudaMalloc((void **)&d_b, size);
  cudaMalloc((void **)&d_c, size);

  // Alloc space for host copies of a, b, c and setup input values

  a = (int *)malloc(size); random_ints(a,N);
  b = (int *)malloc(size); random_ints(b, N);
  c = (int *)malloc(size);

  // Coppy inputs to device

  cudaMemcpy(d_a, a, size, cudaMemcpyHostToDevice);
  cudaMemcpy(d_b, b, size, cudaMemcpyHostToDevice);

  add<<N,1>>>(d_a, d_b, d_c);

  cudaMemcpy(c, d_c, size, cudaMemcpyDeviceToHost);
  cudaFree(d_a); cudaFree(d_c); cudaFree(d_c);
  for(int i = 0; i < 5; i++)
  {

    printf("%d + %d = %d", a[i], b[i], c[i]);

  }

  free(a); free(b); free(c);


  

  return 0;

 
}
