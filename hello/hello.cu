#include <stdio.h>

__global__ void helloGpu()
{
  printf("Hello from the GPU!!\n");
}

int main()
{

  helloGpu<<<1,1>>>();
  cudaDeviceSynchronize();

  return 0;


}
