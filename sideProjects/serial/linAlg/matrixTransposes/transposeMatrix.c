#include <stdlib.h>
#include <stdio.h>
#include <time.h>

int main()
{
    srand(time(NULL));

    int n = rand() % 2 +2;
    int m = rand() % 2 + 2;

    int matrix[m][n];
    
    for(int i = 0; i < m; i++)
    {
        for(int j = 0; j < n; j++)
        {
            matrix[i][j] = (rand() % 19) - 9;
        }
    }
    
    printf("%i x %i Matrix\n", m, n);

    for(int i = 0; i < m; i++)
    {
        for(int j = 0; j < n; j++)
        {
            printf("%i ", matrix[i][j]);
        }
        printf("\n");
    }

    printf("Transposed Matrix\n");

    for(int u = 0; u < n;u++)
    {
        for(int v = 0; v < m; v++)
        {
            printf("%i ", matrix[v][u]);
        }
        printf("\n");
    }

    return 0;

}

