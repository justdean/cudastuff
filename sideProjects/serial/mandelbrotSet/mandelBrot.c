#include <stdio.h>
#include <stdlib.h>
#include <math.h>
//#include <png.h>

#define delta 0.1
#define N 100
#define N_MAX 1600

int mandelBrot(float real, float imag);

int main()
{
  int niter[N_MAX], i = 0;
  float start = -2;
  //unsigned char *rgb = (unsigned char*) calloc(3*N_MAX, sizeof(unsigned char));
  //unsigned char *alpha = (unsigned char*) calloc(N_MAX, sizeof(unsigned char));


  for(int i = 0; i <= 40; i++)
    { 
      for(int j = 0; j <= 40; j++)
        {
          for(int k = 0; k < N_MAX; k++)
            {
              niter[k] = mandelBrot(start + i*delta, start + j*delta);
            }
        }
      }
  printf("Done\n");
  return 0;
}

int mandelBrot(float real, float imag)
{
  float real_n = 0, imag_n = 0, real_i, imag_i;

  for(int n = 0; n < N; n++)
    {
      real_i = (pow(real_n, 2) - pow(imag_n, 2)) + real;
      imag_i = (2*real_n*imag_n) + imag;
      real_n = real_i;
      imag_n = imag_i;

      if((pow(real_i, 2) + pow(imag_i, 2)) > 4)
      {
        return n; //Not in set
      }
    }

  return N ; // In set
}

