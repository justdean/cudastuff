#include <stdio.h>
#include <math.h>

double F(double x);

int main()
{
  double a =1, b = 2, n = 100000;
  double approx, dx, xi, xn = a, xn_next, fx = 0;
  dx = (b-a)/n;

  for(int i = 0; i < n; i++)
    {
      xn_next = xn +dx;
      xi = (xn + xn_next)/2;
      fx += F(xi);
      xn = xn_next;
    }

  approx = dx*fx;
  printf("%lf\n", approx);

  return 0;
}

double F(double x)
{
  return pow(x, 4);
}

