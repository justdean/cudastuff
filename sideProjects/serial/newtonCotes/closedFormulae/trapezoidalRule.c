#include <stdio.h>
#include <math.h>

double F(double x);

int main()
{
  int a = 1, b = 2;
  double n = 10000, dx, xi = 0, xo, xn, x = a;

  dx = (b-a)/n;
  for(int i = 0; i < n-1; i++)
    {
      x += dx;
      double fx = F(x);
      xi += 2*fx;

    }

  xo = F(a);
  xn = F(b);
  double approx = (dx*0.5)*(xo+xi+xn);
  printf("%lf\n", approx);

  return 0;
}

double F(double x)
{
  return pow(x, 4)+5;
}

