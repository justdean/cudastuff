#include <math.h> 
#include <stdio.h>

double F(double x)
{
  return pow(x, 4) + 5;
}

int main()
{

  int a = 1, b = 2, m = 4;
  double n = 100000000, dx, x0, xf, xn = 0, approx,  xi;

  dx = (b-a)/n;

  x0 = F(a);

  for(int i = 0; i < n-1; i++)
    {

      xi = a + i*dx;
      xn += m*F(xi);
      m = (m == 4 ? 2 : 4);

    }

  xf = F(b);

  approx = (dx/3)*(x0 + xn + xf);
  printf("%lf\n", approx);

  return 0;
}

